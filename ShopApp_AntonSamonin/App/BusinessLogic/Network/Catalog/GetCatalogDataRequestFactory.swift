//
//  GetCatalogDataFactory.swift
//  ShopApp_AntonSamonin
//
//  Created by Anton Samonin on 4/25/19.
//  Copyright © 2019 Samonin. All rights reserved.
//

import Foundation
import Alamofire

protocol GetCatalogDataRequestFactory {
    func getCatalogData(pageNumber: Int, categoryId: Int, completionHandler: @escaping (DataResponse<[Product]>) -> Void)
}
