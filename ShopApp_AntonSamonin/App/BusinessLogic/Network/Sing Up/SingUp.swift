//
//  SingUp.swift
//  ShopApp_AntonSamonin
//
//  Created by Anton Samonin on 4/23/19.
//  Copyright © 2019 Samonin. All rights reserved.
//

import Foundation
import Alamofire

class SingUp: AbstractRequestFactory {
   
    var errorParser: AbstractErrorParser
    var sessionManager: SessionManager
    var queue: DispatchQueue?
    let baseUrl = URL(string: "https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/")!
    
    init(
        errorParser: AbstractErrorParser,
        sessionManager: SessionManager,
        queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }
}

extension SingUp: SingUpRequestFactory {
    
    func singUp(userId: Int, userName: String, password: String, email: String, gender: String, creditCard: String, bio: String, completionHandler: @escaping (DataResponse<SingUpResult>) -> Void) {
        let requestModel = SingUpRequestModel(baseUrl: baseUrl, userId: userId, userName: userName, password: password, email: email, gender: gender, creditCard: creditCard, bio: bio)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
}

extension SingUp {
    
    struct SingUpRequestModel: RequestRouter {
       
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = ""
        
        let userId: Int
        let userName: String
        let password: String
        let email: String
        let gender: String
        let creditCard: String
        let bio: String
        
        var parameters: Parameters? {
            return [
                "id_user" : userId,
                "username" : userName,
                "password" : password,
                "email" : email,
                "gender" : gender,
                "credit_card" : creditCard,
                "bio" : bio
            ]
        }
        
        
    }

}

