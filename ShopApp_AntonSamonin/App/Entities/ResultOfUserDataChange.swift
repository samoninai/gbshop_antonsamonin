//
//  ResultOfUserDataChange.swift
//  ShopApp_AntonSamonin
//
//  Created by Anton Samonin on 4/23/19.
//  Copyright © 2019 Samonin. All rights reserved.
//

import Foundation

struct ResultOfUserDataChange: Codable {
    let result: Int
}
