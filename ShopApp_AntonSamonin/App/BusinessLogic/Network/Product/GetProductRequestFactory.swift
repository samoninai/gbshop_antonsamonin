//
//  GetProductRequestFactory.swift
//  ShopApp_AntonSamonin
//
//  Created by Anton Samonin on 4/26/19.
//  Copyright © 2019 Samonin. All rights reserved.
//

import Foundation
import Alamofire

protocol GetProductRequestFactory {
    func getProduct(productId: Int, completionHandler: @escaping (DataResponse<Product>) -> Void)
}
