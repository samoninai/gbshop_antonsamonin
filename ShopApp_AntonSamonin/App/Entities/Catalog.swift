//
//  Catalog.swift
//  ShopApp_AntonSamonin
//
//  Created by Anton Samonin on 4/25/19.
//  Copyright © 2019 Samonin. All rights reserved.
//

import Foundation

struct Catalog: Codable {
    let pageNumber: Int
    let products: [Product]
    
    enum CodingKeys: String, CodingKey {
        case pageNumber = "page_number"
        case products
    }
}
