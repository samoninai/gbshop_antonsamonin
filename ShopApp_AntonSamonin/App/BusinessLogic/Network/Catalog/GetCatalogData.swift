//
//  CatalogData.swift
//  ShopApp_AntonSamonin
//
//  Created by Anton Samonin on 4/25/19.
//  Copyright © 2019 Samonin. All rights reserved.
//

import Foundation
import Alamofire

class GetCatalogData: AbstractRequestFactory {
    
    var errorParser: AbstractErrorParser
    var sessionManager: SessionManager
    var queue: DispatchQueue?
    let baseUrl = URL(string: "https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/")!
    
    init(
        errorParser: AbstractErrorParser,
        sessionManager: SessionManager,
        queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }
}

extension GetCatalogData: GetCatalogDataRequestFactory {
    func getCatalogData(pageNumber: Int, categoryId: Int, completionHandler: @escaping (DataResponse<[Product]>) -> Void) {
        let requestModel = CatalogRequestModel(baseUrl: baseUrl, method: .get, path: "catalogData.json", pageNumber: pageNumber, categoryId: categoryId)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
}

extension GetCatalogData {
    struct CatalogRequestModel: RequestRouter  {
       
        var baseUrl: URL
        var method: HTTPMethod
        var path: String = "catalogData.json"
      
        let pageNumber: Int
        let categoryId: Int
       
        var parameters: Parameters? {
            return [
                "page_number" : pageNumber,
                "id_category" : categoryId
            ]
        }
    }
}
