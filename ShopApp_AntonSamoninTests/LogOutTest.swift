//
//  LogOutTest.swift
//  ShopApp_AntonSamoninTests
//
//  Created by Anton Samonin on 4/26/19.
//  Copyright © 2019 Samonin. All rights reserved.
//

import XCTest
@testable import ShopApp_AntonSamonin

class LogOutTest: XCTestCase {

    let expectation = XCTestExpectation(description: "")
    var errorParser: ErrorParser!
    var requestFactory: RequestFactory!
    
    override func setUp() {
        errorParser = ErrorParser()
        requestFactory = RequestFactory()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        errorParser = nil
        requestFactory = nil
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testLogOut() {
        let logoutRequestFactory = requestFactory.makeLogoutRequestFactory()
        logoutRequestFactory.logout(userId: 123) { [weak self] (respose) in
            switch respose.result {
            case .success(_):
                break
            case .failure(_):
                XCTFail()
            }
            self?.expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10)
    }
}
