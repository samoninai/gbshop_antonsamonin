//
//  LoginResult.swift
//  ShopApp_AntonSamonin
//
//  Created by Anton Samonin on 4/22/19.
//  Copyright © 2019 Samonin. All rights reserved.
//

import Foundation

struct LoginResult: Codable {
    let result: Int
    let user: User
}
