//
//  ChangeUserDataFactory.swift
//  ShopApp_AntonSamonin
//
//  Created by Anton Samonin on 4/23/19.
//  Copyright © 2019 Samonin. All rights reserved.
//

import Foundation
import Alamofire

protocol ChangeUserDataFactory {
    func changeUserInfo(userId: Int, userName: String, password: String, email: String, gender: String, creditCard: String, bio: String, completionHandler: @escaping (DataResponse<ResultOfUserDataChange>) -> Void)
}
