//
//  GetProduct.swift
//  ShopApp_AntonSamonin
//
//  Created by Anton Samonin on 4/26/19.
//  Copyright © 2019 Samonin. All rights reserved.
//

import Foundation
import Alamofire

class GetProduct: AbstractRequestFactory {
    
    var errorParser: AbstractErrorParser
    var sessionManager: SessionManager
    var queue: DispatchQueue?
    let baseUrl = URL(string: "https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/")!
    
    init(
        errorParser: AbstractErrorParser,
        sessionManager: SessionManager,
        queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }
}

extension GetProduct: GetProductRequestFactory {
    func getProduct(productId: Int, completionHandler: @escaping (DataResponse<Product>) -> Void) {
        let requestModel = ProductRequestModel(baseUrl: baseUrl, method: .get, path: "", productId: productId)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
}

extension GetProduct {
    struct ProductRequestModel: RequestRouter {
        var baseUrl: URL
        var method: HTTPMethod
        var path: String
        let productId: Int
        var parameters: Parameters? {
            return ["id_product" : productId]
        }
    }
}
