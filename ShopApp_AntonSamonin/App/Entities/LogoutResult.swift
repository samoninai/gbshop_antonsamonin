//
//  LogoutResult.swift
//  ShopApp_AntonSamonin
//
//  Created by Anton Samonin on 4/23/19.
//  Copyright © 2019 Samonin. All rights reserved.
//

import Foundation

struct LogoutResult: Codable {
    let result: Int
}
