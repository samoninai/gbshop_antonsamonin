//
//  Product.swift
//  ShopApp_AntonSamonin
//
//  Created by Anton Samonin on 4/25/19.
//  Copyright © 2019 Samonin. All rights reserved.
//

import Foundation

struct Product: Codable {
    let productId: Int?
    let productName: String
    let price: Int
    let productDescription: String?
    
    enum CodingKeys: String, CodingKey {
        case productId = "id_product"
        case productName = "product_name"
        case price
        case productDescription = "product_description"
    }
    
}
