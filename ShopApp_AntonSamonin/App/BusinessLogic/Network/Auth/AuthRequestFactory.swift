//
//  AuthRequestFactory.swift
//  ShopApp_AntonSamonin
//
//  Created by Anton Samonin on 4/22/19.
//  Copyright © 2019 Samonin. All rights reserved.
//

import Foundation
import Alamofire

protocol AuthRequestFactory {
    func login(userName: String, password: String, completionHandler: @escaping (DataResponse<LoginResult>) -> Void)
}
